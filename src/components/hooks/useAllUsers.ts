import axios from "axios";
import { IUser } from "components/types/api/user";
import { useCallback, useState } from "react";

import { useMessage } from "./useMessage";

interface IResponse {
  getUsers: () => void;
  loading: boolean;
  users: IUser[];
}

export const useAllUsers = (): IResponse => {
  const { showMessage } = useMessage();
  const [loading, setLoding] = useState<boolean>(false);
  const [users, setUsers] = useState<IUser[]>([]);
  const getUsers = useCallback(() => {
    setLoding(true);
    axios
      .get<IUser[]>("https://jsonplaceholder.typicode.com/users")
      .then((res) => setUsers(res.data))
      .catch(() => {
        showMessage({ title: "ユーザー取得に失敗しました。", status: "error" });
      })
      .finally(() => {
        setLoding(false);
      });
  }, []);

  return { getUsers, loading, users };
};
