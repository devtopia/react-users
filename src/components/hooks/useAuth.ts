/* eslint-disable no-unused-vars */
import axios from "axios";
import { useCallback, useState } from "react";
import { useHistory } from "react-router-dom";

import { useLoginUser } from "./useLoginUser";
import { useMessage } from "./useMessage";

interface IResponse {
  login: (id: string) => void;
  loading: boolean;
}

export const useAuth = (): IResponse => {
  const history = useHistory();
  const { showMessage } = useMessage();
  const { setLoginUser } = useLoginUser();

  const [loading, setLoading] = useState(false);

  const login = useCallback(
    (id: string) => {
      setLoading(true);

      axios
        .get(`https://jsonplaceholder.typicode.com/users/${id}`)
        .then((res) => {
          if (res.data !== undefined) {
            const isAdmin = res.data.id === 10;
            setLoginUser({ ...res.data, isAdmin });
            showMessage({ title: "ログインしました", status: "success" });
            history.push("/home");
          } else {
            showMessage({ title: "ユーザーが見つかりません。", status: "error" });
          }
        })
        .catch(() => {
          showMessage({ title: "ログインできません。", status: "error" });
        })
        .finally(() => {
          setLoading(false);
        });
    },
    [history, showMessage, setLoginUser]
  );
  return { login, loading };
};
