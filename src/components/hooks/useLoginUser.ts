import { useContext } from "react";

import { ILoginUserContext, LoginUserContext } from "../../providers/LoginUserProvider";

export const useLoginUser = (): ILoginUserContext => useContext(LoginUserContext);
