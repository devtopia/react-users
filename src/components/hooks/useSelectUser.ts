import { IUser } from "components/types/api/user";
import { useCallback, useState } from "react";

interface Props {
  id: number;
  users: IUser[];
  onOpen: () => void;
}

interface IResponse {
  onSelectUser: (props: Props) => void;
  selectedUser: IUser | undefined;
}
export const useSelectUser = (): IResponse => {
  const [selectedUser, setSelectedUser] = useState<IUser>();
  const onSelectUser = useCallback((props: Props) => {
    const { id, users, onOpen } = props;
    const targetUser = users.find((user) => user.id === id);
    setSelectedUser(targetUser);
    onOpen();
  }, []);
  return { onSelectUser, selectedUser };
};
