import React, { VFC, memo } from "react";

export const Home: VFC = memo(() => {
  return <p>Homeページです。</p>;
});
Home.displayName = "Home";
