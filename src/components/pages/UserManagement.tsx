import { Center, Spinner, Wrap, WrapItem, useDisclosure } from "@chakra-ui/react";
import { useAllUsers } from "components/hooks/useAllUsers";
import { useLoginUser } from "components/hooks/useLoginUser";
import { useSelectUser } from "components/hooks/useSelectUser";
import { UserCard } from "components/organisms/user/UserCard";
import { UserDetailModal } from "components/organisms/user/UserDetailModal";
import React, { memo, useCallback, useEffect } from "react";

export const UserManagement = memo(() => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { getUsers, users, loading } = useAllUsers();
  const { onSelectUser, selectedUser } = useSelectUser();
  const { loginUser } = useLoginUser();
  console.log(loginUser);

  useEffect(() => getUsers(), []);

  const onClickUser = useCallback(
    (id: number) => {
      console.log(id);
      onSelectUser({ id, users, onOpen });
    },
    [users]
  );

  return (
    <>
      {loading ? (
        <Center h="100vh">
          <Spinner />
        </Center>
      ) : (
        <Wrap p={{ base: 4, md: 10 }}>
          {users.map((user) => (
            <WrapItem key={user.id} mx="auto">
              <UserCard
                id={user.id}
                imageUrl="https://picsum.photos/300/300"
                userName={user.username}
                fullName={user.name}
                onClick={onClickUser}
              />
            </WrapItem>
          ))}
        </Wrap>
      )}
      <UserDetailModal user={selectedUser} isOpen={isOpen} isAdmin={loginUser?.isAdmin} onClose={onClose} />
    </>
  );
});
UserManagement.displayName = "UserManagement";
