import React, { ReactElement, memo } from "react";

import { Header } from "../organisms/layout/Header";

interface Props {
  children: React.ReactNode;
}

export const HeaderLayout = memo((props: Props): ReactElement => {
  const { children } = props;

  return (
    <>
      <Header />
      {children}
    </>
  );
});
HeaderLayout.displayName = "HeaderLayout";
