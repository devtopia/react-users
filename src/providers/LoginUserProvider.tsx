import { IUser } from "components/types/api/user";
import React, { Dispatch, ReactElement, ReactNode, SetStateAction, createContext, useState } from "react";

type LoginUser = IUser & { isAdmin: boolean };

export interface ILoginUserContext {
  loginUser: LoginUser | undefined;
  setLoginUser: Dispatch<SetStateAction<LoginUser | undefined>>;
}

export const LoginUserContext = createContext<ILoginUserContext>({} as ILoginUserContext);

export const LoginUserProvider = (props: { children: ReactNode }): ReactElement => {
  const { children } = props;
  const [loginUser, setLoginUser] = useState<LoginUser | undefined>();
  return <LoginUserContext.Provider value={{ loginUser, setLoginUser }}>{children}</LoginUserContext.Provider>;
};
